// Assignment 1.cpp : Defines the entry point for the console application.
//Linked list

#include "stdafx.h"
#include "LinkedLibrary.hpp"
#include "BinaryTree.hpp"
#include "Unit.hpp"

int _tmain(int argc, _TCHAR* argv[])
{
	LinkedList<int>* v_myList = new LinkedList<int>;
	BinaryTree<int>* v_myTree = new BinaryTree<int>;
	

	v_myList->pushBack(1);
	v_myList->pushBack(5);
	v_myList->pushFront(3);
	v_myList->pushBack(12);
	std::cout << verify(true, v_myList->find(3), "Find 3 in list.") << std::endl;
	std::cout << verify(true, v_myList->find(5), "Find 5 in list.") << std::endl;
	std::cout << verify(false, v_myList->find(21), "21 not present in list.") << std::endl;
	v_myList->pushFront(18);
	v_myList->pushBack(8);
	std::cout << verify(6, v_myList->size(), "Size of list is 6.") << std::endl;
	v_myList->popBack();
	v_myList->popFront();
	std::cout << verify(4, v_myList->size(), "Size after popBack and popFront is 4.") << std::endl;
	v_myList->clear();
	std::cout << verify(0, v_myList->size(), "Size after clear is 0.") << std::endl;

	v_myTree->Insert(6);
	v_myTree->Insert(2);
	v_myTree->Insert(7);
	v_myTree->Insert(1);
	std::cout << verify(4, v_myTree->size(), "Size of tree is 4.") << std::endl;
	v_myTree->Insert(4);
	v_myTree->Insert(9);
	v_myTree->Insert(3);
	std::cout << verify(7, v_myTree->size(), "Size of tree is 7.") << std::endl;
	v_myTree->Insert(5);
	v_myTree->Insert(8);
	std::cout << verify(true, v_myTree->find(4), "Find 4 in tree.") << std::endl;
	std::cout << verify(false, v_myTree->find(456), "456 is not present in tree") << std::endl;

	v_myTree->traversal_preOrder();
	v_myTree->traversal_inOrder();
	v_myTree->traversal_postOrder();

	v_myTree->clear();
	std::cout << verify(0, v_myTree->size(), "Size after clear is 0.") << std::endl;
	
	std::cin.get();
	return 0;
}