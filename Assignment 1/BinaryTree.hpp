// BinaryTree.hpp

#include "stdafx.h"
#include "Node.hpp"

#pragma once

template <class T>
class BinaryTree
{
public:
	BinaryTree() : head(nullptr)
	{}

	~BinaryTree()
	{
		clear();
	}

	void Insert(T x)
	{
		Node<T> *currentNode = head;

		if (head == nullptr)
		{
			head = new Node<T>();
			head->data = x;
			return;
		}

		while (currentNode != nullptr)
		{
			if (x == currentNode->data)
				return;
			else if (x > currentNode->data)
			{
				if (currentNode->bigger == nullptr)
				{
					currentNode->bigger = new Node < T > ;
					currentNode->bigger->data = x;
				}
				else
				{
					currentNode = currentNode->bigger;
				}
			}
			else
			{
				if (currentNode->smaller == nullptr)
				{
					currentNode->smaller = new Node < T > ;
					currentNode->smaller->data = x;
				}
				else
				{
					currentNode = currentNode->smaller;
				}
			}
		}
	}



	int size()
	{ 
		if (!head)
			return 0;

		int size = 0;
		sizeoftree(head, size);
		return size;
	}

	void clear()
	{
		if (head)
		{
			clear(head);
			head = nullptr;
		}
	}

	bool find(T data)
	{
		Node<T>* currentNode = head;

		if (!head)
			return false;
		if (find(currentNode, data))
			return true;
		else
			return false;
	}

	void traversal_preOrder()
	{
		Node<T>* currentNode = head;
		if (head)
			traversal_preOrder(head);
		std::cout << std::endl;
	}

	void traversal_inOrder()
	{
		Node<T>* currentNode = head;
		if (head)
			traversal_inOrder(currentNode);
		std::cout << std::endl;
	}

	void traversal_postOrder()
	{
		Node<T>* currentNode = head;
		if (head)
			traversal_postOrder(currentNode);
		std::cout << std::endl;
	}

private:
	void traversal_preOrder(Node<T>* currentNode)
	{
		std::cout << currentNode->data << " ";

		if (currentNode->smaller)
			traversal_preOrder(currentNode->smaller);
		if (currentNode->bigger)
			traversal_preOrder(currentNode->bigger);
	}

	void traversal_inOrder(Node<T>* currentNode)
	{
		if (currentNode->smaller)
			traversal_inOrder(currentNode->smaller);
		std::cout << currentNode->data << " ";
		if (currentNode->bigger)
			traversal_inOrder(currentNode->bigger);
	}
	
	void traversal_postOrder(Node<T>* currentNode)
	{
		if (currentNode->smaller)
			traversal_postOrder(currentNode->smaller);
		if (currentNode->bigger)
			traversal_postOrder(currentNode->bigger);
		std::cout << currentNode->data << " ";
	}

	bool find(Node<T>* node, T data)
	{
		if (data == node->data)
			return true;

		else if (data < node->data)
		{
			if (node->smaller)
			{
				node = node->smaller;
				find(node, data);
			}
			else
				return false;
		}

		else if (data > node->data)
		{
			if (node->bigger)
			{
				node = node->bigger;
				find(node, data);
			}
			else
				return false;
		}
	}

	void clear(Node<T>* node)
	{
		if (!node)
		{
			clear(node->bigger);
			clear(node->smaller);
			node = nullptr;
			delete node;
		}
	}

	void sizeoftree( Node<T>* node, int &size)
	{
		if (node)
		{
			size++;
			if (node->bigger)
				sizeoftree(node->bigger, size);
			if (node->smaller)
				sizeoftree(node->smaller, size);
		}
	}

	Node<T> *head;
};