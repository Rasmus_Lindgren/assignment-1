#include "stdafx.h"
#include "Node.hpp"

#pragma once

//template <typename Type>								//Type kan va vilken datatyp som helst
//struct Node
//{
//	Type data;											//Type kan kallas som data
//	Node<Type> *next = nullptr;
//};

template <class T>
class LinkedList
{
public:
	LinkedList() : head(nullptr)							//roten initieras som null
	{}

	~LinkedList()
	{
		clear();
	}

	int size() const
	{
		if (!head)
		{
			return 0;
		}

		int count = 0;										//initierar count som noll, vi r�knar h�rifr�n.
		Node<T> *node = head;								//En node-pekare som �r roten av listan (f�rsta)
		while (node)										//S� l�nge node �r n�gonting.
		{
			count++;										//�ka count med 1
			node = node->next;								//Node blir n�sta node i listan.
		}													//Den forts�tter loopa och �ka count tills node inte kan bli n�sta node i listan
		return count;										//Ge tillbaka v�rdet f�r count som storleken p� listan
	}

	void pushFront(T value)
	{
		Node<T> *newNode = new Node<T>;
		newNode->data = value;
		newNode->next = head;
		head = newNode;
	}

	void pushBack(T value)									//L�gg till ett element l�ngst bak i listan
	{
		Node<T> *newNode = new Node < T > ;					//Skapa en ny node
		newNode->data = value;										//Den nya noden pekar mot datatypen som Type �r

		if (head)											//Om vi �r p� roten
		{
			Node<T> *node = head;							//En node-pekare som �r roten
			while (node->next)								//
			{
				node = node->next;							//
			}

			node->next = newNode;
		}
		else
		{
			head = newNode;
		}
	}

	void popFront()
	{
		Node<T> *popNode = head;
		head = popNode->next;
		delete popNode;
	}

	void popBack()
	{
		if (!head->next)
		{
			delete head;
			head = nullptr;
			return;
		}

		Node<T> *popNode = head;
		while (popNode->next->next)
		{
			popNode = popNode->next;
		}
		delete popNode->next;
		popNode->next = nullptr;
	}

	void clear()
	{
		while (head)
		{
			popBack();
		}
	}

	bool find(T nodeData)
	{
		Node<T> *findNode = head;

		while (findNode)
		{
			if (nodeData == findNode->data)
			{
				return true;
			}
			else
			{
				findNode = findNode->next;
			}

		}
		return false;
	}

private:

	Node<T> *head;
};

