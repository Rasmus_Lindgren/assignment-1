//Node.hpp
#include "stdafx.h"
#pragma once

template <typename Type>								//Type kan va vilken datatyp som helst
struct Node
{
	Type data;											//Type kan kallas som data
	Node<Type> *bigger = nullptr;
	Node<Type> *smaller = nullptr;
	Node<Type> *next = nullptr;
};